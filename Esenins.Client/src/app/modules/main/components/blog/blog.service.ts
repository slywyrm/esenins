import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { pluck, } from 'rxjs/operators';
import { FacebookPost } from '../../../shared/models/facebook-post';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  constructor(private http: HttpClient) { }

  getPosts(): Observable<FacebookPost[]> {
    return this.http.get<FacebookPost[]>('/Facebook/posts').pipe(
      pluck('data')
    );
  }
}
