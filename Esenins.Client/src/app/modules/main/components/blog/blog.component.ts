import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { FacebookPost } from '../../../shared/models/facebook-post';
import * as Packery from 'packery';
import { MainService } from '../../main.service';
import { tap } from 'rxjs/operators';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'es-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  @ViewChild('shuffle') private shuffleElement: ElementRef;
  private packery: Packery;
  private postsNumber = 0;
  posts: FacebookPost[];

  get menuShown$(): Observable<boolean> {
    return this.mainService.menuShown$.pipe(
      tap(this.repack.bind(this))
    );
  }

  constructor(private mainService: MainService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data
      .subscribe(data => this.posts = data.posts);
  }

  postInitiated() {
    if (++this.postsNumber >= this.posts.length) {
      this.packery = new Packery(this.shuffleElement.nativeElement, {
        columnWidth: '.sizer',
        itemSelector: '.fb-post',
        stagger: 50,
        percentPosition: true
      });
    }
  }

  repack(): void {
    if (this.packery) {
      setTimeout(() => this.packery.layout());
    }
  }

  shiftLayout(): void {
    if (this.packery) {
      this.packery.shiftLayout();
    }
  }

}
