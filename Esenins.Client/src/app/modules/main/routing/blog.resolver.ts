import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { FacebookPost } from "../../shared/models/facebook-post";
import { BlogService } from "../components/blog/blog.service";
import { Observable } from "rxjs/internal/Observable";
import { Injectable } from "@angular/core";

@Injectable()
export class BlogResolver implements Resolve<FacebookPost[]> {
  constructor(private blogService: BlogService) { }

  resolve(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FacebookPost[]> {
    return this.blogService.getPosts();
  }
}
