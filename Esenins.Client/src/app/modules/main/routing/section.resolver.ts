import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { MainService } from '../main.service';
import { DOCUMENT } from '@angular/common';

@Injectable()
export class SectionResolver implements Resolve<any> {

  constructor(private mainService: MainService,
              @Inject(DOCUMENT) private document: Document) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
    this.mainService.changeSelectedSection(route.url[0].path);
    setTimeout(() => this.document.scrollingElement.scrollTop = 0, 400);
    return new Promise(resolve => resolve());
  }
}
