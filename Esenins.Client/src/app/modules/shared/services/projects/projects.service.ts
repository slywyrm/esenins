import { Injectable } from '@angular/core';
import { PortfolioItem } from '../../models/portfolio-item.model';
import { ProjectsBySections } from '../../models/project-section';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ProjectsService {
  constructor(private http: HttpClient) { }

  getPortfolio(): Observable<PortfolioItem[]> {
    return this.http.get<PortfolioItem[]>('/projects/portfolio');
  }

  getPortfolioItem(id: string): Observable<PortfolioItem> {
    return this.http.get<PortfolioItem>(`/projects/portfolio/${id}`);
  }

  getProjects(): Observable<ProjectsBySections> {
    return this.http.get<ProjectsBySections>('/projects');
  }

}
