import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FadeDirective } from './directives/fade/fade.directive';
import { SlidesService } from './services/slides/slides.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ContactsService } from './services/contacts/contacts.service';
import { ProjectsService } from './services/projects/projects.service';
import { CopyrightComponent } from './components/copyright/copyright.component';
import { SectionStaticComponent } from './components/section-static/section-static.component';
import { TouchScrollDirective } from './directives/touch-scroll.directive';
import { HoverClassDirective } from './directives/hover-class.directive';
import { ApiUrlInterceptor } from './http-interceptors/api-url-interceptor.service';
import { CachingInterceptor } from "./http-interceptors/cache-interceptor.service";
import { RequestCache, RequestCacheWithMap } from "./services/request-cache.service";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [FadeDirective, CopyrightComponent, SectionStaticComponent, TouchScrollDirective, HoverClassDirective],
  exports: [FadeDirective, CopyrightComponent, SectionStaticComponent, TouchScrollDirective, HoverClassDirective]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        SlidesService,
        ContactsService,
        ProjectsService,
        { provide: HTTP_INTERCEPTORS, useClass: ApiUrlInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: CachingInterceptor, multi: true },
        { provide: RequestCache, useClass: RequestCacheWithMap }
      ]
    };
  }
}
