import { ImageSrc } from './image-src.type';
import { Copyright } from './copyright';

export type TileSize = 'long' | 'tall' | 'big';

export interface PorfolioItemPhoto {
  id: string;
  path: string;
  order: number;
}

export interface PortfolioItem {
  id: string;
  name: string;
  subName: string;
  annotation: string;
  description: string;
  placeholderPhoto: ImageSrc;
  tileSize?: TileSize;
  photos: PorfolioItemPhoto[];
  copyright?: Copyright;
}
