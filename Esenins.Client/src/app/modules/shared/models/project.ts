import { Copyright } from './copyright';

export interface Project {
  name: string;
  portfolioId?: string;
  copyright?: Copyright;
}
