import { ImageSrc } from './image-src.type';

export interface Copyright {
  name: string;
  logo?: ImageSrc;
}
