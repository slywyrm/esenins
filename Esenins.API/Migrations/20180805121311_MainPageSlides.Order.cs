﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Esenins.API.Migrations
{
    public partial class MainPageSlidesOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "order",
                table: "main_page_slides",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "order",
                table: "main_page_slides");
        }
    }
}
