﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Esenins.API.Migrations
{
    public partial class ImageOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "order",
                table: "images",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "order",
                table: "images");
        }
    }
}
