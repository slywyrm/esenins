﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Esenins.API.Data;
using Esenins.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Esenins.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectsRepository _repository;

        public ProjectsController(IProjectsRepository repository)
        {
            _repository = repository;
        }
        
        [HttpGet("portfolio")]
        public async Task<ActionResult<List<PortfolioItem>>> GetPortfolio()
        {
            return await _repository.GetPortfolio();
        }

        [HttpGet("portfolio/{id:guid}")]
        public async Task<ActionResult<PortfolioItem>> GetPortfolioItem(Guid id)
        {
            return await _repository.GetPortfolioItem(id);
        }

        [HttpGet("")]
        public async Task<ActionResult<Dictionary<string, ProjectsSection>>> GetProjects()
        {
            return await _repository.GetProjects();
        }
    }
}