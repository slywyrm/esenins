﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Esenins.API.Data;
using Esenins.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Esenins.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MainPageController : ControllerBase
    {
        private readonly IMainPageSlidesRepository _repository;

        public MainPageController(IMainPageSlidesRepository repository)
        {
            _repository = repository;
        }
        
        [HttpGet("slides")]
        public async Task<ActionResult<List<MainPageSlide>>> GetSlides()
        {
            return await _repository.GetSlides();
        }
    }
}