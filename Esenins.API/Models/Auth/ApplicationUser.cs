﻿using Microsoft.AspNetCore.Identity;

namespace Esenins.API.Models.Auth
{
    public class ApplicationUser : IdentityUser
    {
        
    }
}