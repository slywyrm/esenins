﻿using System;

namespace Esenins.API.Models
{
    public class Copyright
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
    }
}