﻿using System;

namespace Esenins.API.Models
{
    public class Image
    {
        public Guid Id { get; set; }
        public string Path { get; set; }
        public int Order { get; set; }
    }
}