﻿using System.IO;
using System.Threading.Tasks;

namespace Esenins.API.Data
{
    public interface IFacebookConnector
    {
        Task<Stream> GetPosts();
    }
}