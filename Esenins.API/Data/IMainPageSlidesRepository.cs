﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Esenins.API.Models;

namespace Esenins.API.Data
{
    public interface IMainPageSlidesRepository
    {
        Task<List<MainPageSlide>> GetSlides();
    }
}