﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;

namespace Esenins.API.Data
{
    [DataContract]
    class FacebookCredentials
    {
        [DataMember(Name="access_token")]
        public string access_token;
    }
    
    public class FacebookConnector : IFacebookConnector
    {
        private HttpClient _http = new HttpClient();
        private string _token = "EAADXPcdSZBKIBAIk3vIgpNFJVi3442VPl6TqYa7YvzGeLdqYf5PlVlQmK8BDweZCtuKBHwXtthKj2qSJ2YIoZBOkwSE7b2cq8ZB41JlmFMx4Lm9EJuU7EqjQPvbuScBXPjifGZCHqfh0TeBbKhoUpAWo0WkvoeWWNsaWO9qO8aZBKudeCBtZC7RUdl2fnsAIZCwRT7i7eNSHFAZDZD";

        public FacebookConnector()
        {
            _http.BaseAddress = new Uri("https://graph.facebook.com/");
            _http.DefaultRequestHeaders.Accept.Clear();
            _http.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            // GetToken().GetAwaiter().GetResult();
        }

        private async Task GetToken()
        {
            var response = await _http.GetAsync(
                "oauth/access_token?client_id=236660337080482&client_secret=19c1b7307870d3ffd03d28dd01c130d3&grant_type=client_credentials");
            if (response.IsSuccessStatusCode)
            {
                _token = (await response.Content.ReadAsAsync<FacebookCredentials>()).access_token;
            }
        }

        public async Task<Stream> GetPosts()
        {
            var response = await _http.GetAsync($"2022032894724732/feed?fields=permalink_url,message,shares,full_picture,link,updated_time&access_token={_token}&limit=15");
            var content = await response.Content.ReadAsStreamAsync();
            return content;
        }

    }
}