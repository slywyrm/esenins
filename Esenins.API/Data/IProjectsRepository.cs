﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Esenins.API.Models;

namespace Esenins.API.Data
{
    public interface IProjectsRepository
    {
        Task<List<PortfolioItem>> GetPortfolio();
        Task<PortfolioItem> GetPortfolioItem(Guid id);
        Task<Dictionary<string, ProjectsSection>> GetProjects();
    }
}