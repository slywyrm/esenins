﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Esenins.API.Models;
using Microsoft.EntityFrameworkCore;

namespace Esenins.API.Data
{
    public class ProjectsRepository : IProjectsRepository
    {
        private AppDbContext _context;

        public ProjectsRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<List<PortfolioItem>> GetPortfolio()
        {
            var result = await _context.Portfolio
                .Include(i => i.Copyright)
                .OrderByDescending(i => i.Order)
                .ToListAsync();
            return result;
        }

        public async Task<PortfolioItem> GetPortfolioItem(Guid id)
        {
            var result = await _context.Portfolio
                .Include(i => i.Photos)
                .Include(i => i.Copyright)
                .SingleAsync(i => i.Id == id);
            result.Photos = result.Photos.OrderBy(p => p.Order).ToList();
            return result;
        }

        public async Task<Dictionary<String, ProjectsSection>> GetProjects()
        {
            var result = await _context.ProjectsBySection.Include(i => i.Projects).ThenInclude(p => p.Copyright)
                .ToDictionaryAsync(i => i.Id, i => i);
            return result;
        }
    }
}