﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Esenins.API.Models;
using Microsoft.EntityFrameworkCore;

namespace Esenins.API.Data
{
    public class MainPageSlidesRepository : IMainPageSlidesRepository
    {
        private AppDbContext _context;

        public MainPageSlidesRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<List<MainPageSlide>> GetSlides()
        {
            var result = await _context.MainPageSlides
                .Include(i => i.Copyright)
                .OrderByDescending(i => i.Order)
                .ToListAsync();
            return result;
        }
    }
}