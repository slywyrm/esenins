﻿using System.IO;
using Esenins.API.Data;
using Esenins.API.Models.Auth;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace Esenins.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        public IConfiguration Configuration { get; }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(
                options => options.SwaggerDoc("v1", new Info {Title = "Esenins API", Version = "v1"}));

            services.AddEntityFrameworkNpgsql()
                .AddDbContext<AppDbContext>(options => options.UseNpgsql(Configuration["PG_CONNECTION_STRING"]));

            // services.AddIdentity<ApplicationUser, IdentityRole>()
            //     .AddEntityFrameworkStores<AppDbContext>()
            //     .AddDefaultTokenProviders();

            // services.AddIdentityServer()
            //     .AddJwtBearerClientAuthentication()
                // .Add
                // .AddAspNetIdentity<ApplicationUser>();

            services.AddSingleton<IFacebookConnector, FacebookConnector>();
            services.AddScoped<IProjectsRepository, ProjectsRepository>();
            services.AddScoped<IMainPageSlidesRepository, MainPageSlidesRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
            
            app.UseSwagger();
            app.UseSwaggerUI(options => options.SwaggerEndpoint("/swagger/v1/swagger.json", "Esenins api v1"));
        }
    }
}